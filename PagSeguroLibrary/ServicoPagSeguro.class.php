<?php
include '../../dbase.php';
include '../../settings.php';
include '../../PagSeguroLibrary/PagSeguroLibrary.php';

class ServicoPagSeguro{    
    private $_token = null;
    private $_email = null;
    private $_paymentRequest = null;
    
    public function __construct(){
        $this->_paymentRequest = new PagSeguroPaymentRequest();        
    }
    
    public function pagar($id){
        $dados = mysql_fetch_array($this->getValueById($id));                
        
        $this->_paymentRequest->addItem($dados['id'], "Pacote ".$dados['name'], 1, $dados['price']);        
        $this->_paymentRequest->setCurrency("BRL");
        $this->_paymentRequest->setReference($id); 
        $this->_paymentRequest->setRedirectUrl("www.sapecasonline.com");
   
        $this->getConfig();       
        $credenciais = new PagSeguroAccountCredentials(
                $this->_email,
                $this->_token
        );
        
        $url = $this->_paymentRequest->register($credenciais);
        header("Location: $url");
        
    }
    
    private function getValueById($id){
        $query = "SELECT * FROM package where id = '$id'";        
        return mysql_query($query);        
    }
    
    public function getConfig(){
        $dados = mysql_fetch_array(mysql_query("SELECT * FROM config_pagseguro limit 1"));
        $this->setEmail($dados['email']);
        $this->setToken($dados['token']);
        
    }
    
    public function setEmail($email){
        $this->_email = $email;
    }
    
    public function setToken($token){
        $this->_token = $token;
    }
}

